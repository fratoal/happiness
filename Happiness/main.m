//
//  main.m
//  Happiness
//
//  Created by Francesc Tovar on 14/07/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HappinessAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HappinessAppDelegate class]));
    }
}

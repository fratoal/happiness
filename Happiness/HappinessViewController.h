//
//  HappinessViewController.h
//  Happiness
//
//  Created by Francesc Tovar on 14/07/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HappinessViewController : UIViewController

@property (nonatomic) int happiness; // 0 is Sad and 100 is very Happy

@end

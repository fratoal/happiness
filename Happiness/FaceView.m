//
//  FaceView.m
//  Happiness
//
//  Created by Francesc Tovar on 14/07/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import "FaceView.h"

@implementation FaceView

@synthesize dataSource = _dataSource;
@synthesize scale = _scale;

#define DEFAULT_SCALE 0.90

- (CGFloat)scale
{
    if (!_scale) {
        return DEFAULT_SCALE;
    } else {
        return _scale;
    }
}

- (void)setScale:(CGFloat)scale
{
    if (scale != _scale) {
        _scale = scale;
        [self setNeedsDisplay];
    }
}

- (void)pinch:(UIPinchGestureRecognizer *)gesture
{
    if ((gesture.state == UIGestureRecognizerStateChanged) || 
        (gesture.state == UIGestureRecognizerStateEnded)) {
        self.scale *= gesture.scale;
        gesture.scale = 1;
    }
}

- (void)setup
{
    self.contentMode = UIViewContentModeRedraw;
    self.backgroundColor = [UIColor blackColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)drawCircleAtPoint:(CGPoint)p withRadius:(CGFloat)radius inContext:(CGContextRef)context
{
    // Recuperamos el contexto
    UIGraphicsPushContext(context);
    
    // Empezamos a dibujar en el contexto
    // Creamos un arco con los parametros pasados
    // Dibujamos un borde o fondo a esta forma
    CGContextBeginPath(context);
    CGContextAddArc(context, p.x, p.y, radius, 0, 2*M_PI, YES);
    //CGContextStrokePath(context);
    CGContextFillPath(context);
    
    // Devolvemos el contexto
    UIGraphicsPopContext();
}

- (void)drawRect:(CGRect)rect
{
    // Lo primero es conseguir el contexto
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Recuperamos el punto medio de la vista
    CGPoint midPoint;
    midPoint.x = self.bounds.origin.x + self.bounds.size.width/2;
    midPoint.y = self.bounds.origin.y + self.bounds.size.height/2;
    
    // Circulo principal --------------------------------------//
    
    // Definimos el tamaño del circulo dependiendo de la posición del dispositivo
    // y lo multiplicamos por la constante de escalado para obtener el tamaño deseado
    CGFloat size = self.bounds.size.width / 2;
    if (self.bounds.size.height < self.bounds.size.width) size = self.bounds.size.height / 2;
    size *= self.scale;
    
    // Definimos el grosor de la linea y el color
    CGContextSetLineWidth(context, 5.0);
    //[[UIColor blackColor] setStroke];
    [[UIColor yellowColor] setFill];
    
    // Y llamamos a la funcion para dibujar el circulo
    [self drawCircleAtPoint:midPoint withRadius:size inContext:context];
    
    // Ojos ---------------------------------------------------//

#define EYE_H 0.40
#define EYE_V 0.25
#define EYE_RADIUS 0.13
    
    CGPoint eyePoint;
    eyePoint.x = midPoint.x - size * EYE_H;
    eyePoint.y = midPoint.y - size * EYE_V;
    
    // Configuramos los colores
    [[UIColor blackColor] setFill];
    
    // Dibujamos el primer ojo
    [self drawCircleAtPoint:eyePoint withRadius:size * EYE_RADIUS inContext:context];
    eyePoint.x += size * EYE_H * 2; // Posicionamos el segundo ojo
    [self drawCircleAtPoint:eyePoint withRadius:size * EYE_RADIUS inContext:context];
    
    // Boca ---------------------------------------------------//
    
#define MOUTH_H 0.45
#define MOUTH_V 0.40
#define MOUTH_SMILE 0.25
    
    CGPoint mouthStart;
    mouthStart.x = midPoint.x - MOUTH_H * size;
    mouthStart.y = midPoint.y + MOUTH_V * size;
    
    CGPoint mouthEnd = mouthStart;
    mouthEnd.x += MOUTH_H * size * 2;
    
    CGPoint mouthCP1 = mouthStart;
    mouthCP1.x += MOUTH_H * size * 2/3;
    
    CGPoint mouthCP2 = mouthEnd;
    mouthCP2.x -= MOUTH_H * size * 2/3;
    
    // El valor de smile se lo pediremos al delegado a través del protocolo
    float smile = [self.dataSource smileForFaceView:self];
    // El valor smile solo adminte valor entre 0 y 1
    if (smile < -1) smile = -1;
    if (smile > 1 ) smile = 1;
    
    CGFloat smileOffset = MOUTH_SMILE * size * smile;
    mouthCP1.y += smileOffset;
    mouthCP2.y += smileOffset;
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, mouthStart.x, mouthStart.y);
    CGContextAddCurveToPoint(context, mouthCP1.x, mouthCP2.y, mouthCP2.x, mouthCP2.y, mouthEnd.x, mouthEnd.y);
    CGContextStrokePath(context);
    
    
}


@end














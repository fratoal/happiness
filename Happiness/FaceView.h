//
//  FaceView.h
//  Happiness
//
//  Created by Francesc Tovar on 14/07/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FaceView; // Forward Reference
                 // Le decimos al compilador que el objeto para el delegado será definido más tarde

@protocol FaceViewDataSource
- (float)smileForFaceView:(FaceView *)sender;
@end

@interface FaceView : UIView

@property (nonatomic) CGFloat scale;

- (void)pinch:(UIPinchGestureRecognizer *)gesture;

@property (nonatomic, weak) IBOutlet id <FaceViewDataSource> dataSource;

@end
